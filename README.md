# Spacefarer

## Quick notes for Chris:

For milestones 1-2:\
We just have one scene!

For milestone 3:\
We have a scene called base that we store all of our shared objects and stuff in. This is the scene that should be run to play the game.\
Running one of the other scenes will just look blank!\

For milestone 4:\
We have a title screen, in the "TitleScreen" scene. This is the scene to start with!\

Let us know if you have any questions :)
