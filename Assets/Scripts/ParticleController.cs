using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    public Transform sparkles;
    //public GameObject gem;
   //public Gem collidedGem;

    // Start is called before the first frame update
    [System.Obsolete]
    void Start()
    {
        sparkles.GetComponent<ParticleSystem>().enableEmission = false;
    }

    [System.Obsolete]
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name == "Gem")
        {
            sparkles.GetComponent<ParticleSystem>().enableEmission = true;
            StartCoroutine(stopSparkles());
        }
        else
        {
            sparkles.GetComponent<ParticleSystem>().enableEmission = false;
        }
    }


    [System.Obsolete]
    IEnumerator stopSparkles()
    {
        yield return new WaitForSeconds(.8f);
        sparkles.GetComponent<ParticleSystem>().enableEmission = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
