using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour
{
    public GameManager gameManager; // Reference to gamemanager 

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>(); 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player")) return;

        // Player came back to ship, check if they have enough gems to leave

        Inventory playerInventory =  collision.gameObject.GetComponent<Inventory>();
        if (playerInventory == null)
        {
            Debug.LogError("Inventory not set up.");
            return;
        }

        if (playerInventory.HasEnoughOfItemOfType(gameManager.sceneConfig.numGemsRequired, typeof(Gem)))
        {
            gameManager.playerEscaped.Invoke();
        }
    }
}
