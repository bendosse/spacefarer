using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{

  private bool inventoryOpen = false;
  private GameObject[] gems;
  private GameObject[] hearts;

  // Bunch of references to UI elements
  public GameObject player;
  public GameManager gameManager;
  public GameObject GemList;
  public GameObject AbsentGemPrefab;
  public GameObject PresentGemPrefab;
  public GameObject healthBar;
  public GameObject StartScreen;
  public GameObject StartButton;
  public GameObject InventoryScreen;
  public GameObject ClosestGemArrow;
  public GameObject SpaceshipArrow;
  public GameObject Spaceship;

  // Start is called before the first frame update
  void Start()
  {
    // Only show the start screen at first
    StartScreen.SetActive(true);

    // If the player dies, show the death screen
    gameManager.playerDied.AddListener(() =>
    {
      StartCoroutine(ShowEndScreen());
    });


    // When a new level is loaded, initialize the UI
    gameManager.levelLoaded.AddListener(() =>
    {
      // Remove all old gems from player
      player.GetComponent<Inventory>().RemoveItemsOfType(typeof(Gem));

      // Destroy old UI gems
      if (gems != null)
      {
        foreach (GameObject gem in gems)
        {
          Destroy(gem);
        }
      }

      // Make new UI gems
      gems = new GameObject[gameManager.sceneConfig.numGemsRequired];
      for (int i = 0; i < gameManager.sceneConfig.numGemsRequired; i++)
      {
        gems[i] = Instantiate(AbsentGemPrefab);
        gems[i].transform.SetParent(GemList.transform);
        gems[i].transform.position = new Vector3(GemList.transform.position.x + gems[i].GetComponent<RectTransform>().rect.width * i, GemList.transform.position.y, 0);
      }

      // Track player health
      player.GetComponent<Player>().OnPlayerHealthChanged.AddListener(UpdateHealthBar);

      Spaceship = GameObject.Find("Spaceship");
    });
  }

  private IEnumerator ShowEndScreen()
  {
    // Load end screen scene
    yield return SceneManager.LoadSceneAsync("EndScreen", LoadSceneMode.Single);
  }

  // Change gem on UI to show how many have 
  public void CollectGem()
  {
    int i = player.GetComponent<Inventory>().NumItemsOfType(typeof(Gem)) - 1;
    Vector3 pos = gems[i].transform.position;
    gems[i] = Instantiate(PresentGemPrefab);
    gems[i].transform.SetParent(GemList.transform);
    gems[i].transform.position = pos;
  }

  private void UpdateHealthBar(float newHealth)
  {
    healthBar.GetComponent<Image>().fillAmount = newHealth / gameManager.playerInitialHealth;
  }

  public void CloseStartWindow()
  {
    StartScreen.SetActive(false);
    
    gameManager.StartGame();
  }

  private void Update()
  {
    // Open or close the inventory
    if (Input.GetKeyDown(KeyCode.I) && !inventoryOpen)
    {
      InventoryScreen.GetComponent<InventoryUI>().Init();
      InventoryScreen.SetActive(true);
      inventoryOpen = true;
    }
    else if ((Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.Escape)) && inventoryOpen)
    {
      InventoryScreen.SetActive(false);
      inventoryOpen = false;
    }

    // Update UI arrows to show closest gem and direction to ship
    ClosestGemArrow.transform.rotation = Quaternion.FromToRotation(Vector3.right, gameManager.ClosestGem);
    if (Spaceship == null) return;
    SpaceshipArrow.transform.rotation = Quaternion.FromToRotation(Vector3.right, Spaceship.transform.position - player.transform.position);
  }
}
