using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetTweening : MonoBehaviour
{
    public Vector3 moveTo;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LerpPosition(moveTo, 6));
    }

    IEnumerator LerpPosition(Vector3 targetPosition, float duration)
    {
        float time = 0;
        Vector3 startPosition = transform.position;
        targetPosition = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);

        while (time < duration)
        {
            transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
    }
}
