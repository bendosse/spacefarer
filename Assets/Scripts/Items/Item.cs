using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
  public string name { get; }
  public bool inInventory { get; set; }

  // Reference to the player
  public Player player;

  public Item()
  {
    name = "";
    inInventory = false;
  }

  public Item(string name, bool inInventory)
  {
    this.name = name;
    this.inInventory = inInventory;
  }

  public virtual void Use(Player player)
  {
    Debug.Log("Using " + name);
  }
}
