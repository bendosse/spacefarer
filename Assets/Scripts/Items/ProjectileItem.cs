using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileItem : Item
{
  public Color projectileColor { get; protected set; }
  public float projectileSpeed { get; protected set; }
  public float projectileDamage { get; protected set; }
  public int projectileHits { get; protected set; }

  public ProjectileItem(string name, bool inInventory, 
                        Color projectileColor, float projectileSpeed,
                        float projectileDamage, int projectileHits) : base(name, inInventory)
  {
    this.projectileColor = projectileColor;
    this.projectileSpeed = projectileSpeed;
    this.projectileDamage = projectileDamage;
    this.projectileHits = projectileHits;
  }

  public override void Use(Player player)
  {
    this.player = player;
    player.equippedProjectile = this;
  }
}
