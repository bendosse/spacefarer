using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthItem : Item
{
  public float healthAmount;

  public HealthItem(string name, bool inInventory, float healthAmount) : base(name, inInventory)
  {
    this.healthAmount = healthAmount;
  }
  public override void Use(Player player)
  {
    this.player = player;
    player.Heal(healthAmount);
  }
}
