using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

  private List<GameObject> gems;
  private int sceneIndex;
  private GameObject enemyParent;
  private Coroutine closestGemCoroutine;

  // Scene configuration object, each of our levels has one
  public SceneConfig sceneConfig;
  public float playerSpeed;
  public float playerInitialHealth;
  public UnityEvent playerDied;
  public UnityEvent playerEscaped;
  public UnityEvent levelLoaded;
  public Player player;
  public string[] scenes;
  public Vector3 ClosestGem { get; private set; }

  // Possbile items to drop
  private Item[] dropItems = new Item[]
  {
    new HealthItem("Health Pack", false, 1f),
    new ProjectileItem("Fire Projectile", false, Color.yellow, 10f, 5f, 5),
    new ProjectileItem("Ice Projectile", false, Color.cyan, 15f, 5f, 2),
    new ProjectileItem("Lightning Projectile", false, Color.yellow, 10f, 1f, 10)
  };


  // Start is called before the first frame update
  void Start()
  {
    DontDestroyOnLoad(gameObject);

    // Prevent player from shooting while on intro screen
    player.canShoot = false;

    sceneIndex = 0;
  }

  public void StartGame()
  {
    StartCoroutine(StartCoroutine());
  }

  private IEnumerator StartCoroutine()
  {
    // Wait for first scene to load, use additive scene loading to keep game manager, etc.
    yield return SceneManager.LoadSceneAsync(scenes[sceneIndex], LoadSceneMode.Additive);

    Init();

    player.canShoot = true;

    // When the player escapes, load the next scene
    playerEscaped.AddListener(() =>
    {
      StartCoroutine(LoadNextLevel());
    });
  }

  private IEnumerator LoadNextLevel()
  {
    // Unload current scene
    yield return SceneManager.UnloadSceneAsync(scenes[sceneIndex]);

    sceneIndex++;
    if (sceneIndex >= scenes.Length) // Game is over
    {
      // Load end screen scene
      yield return SceneManager.LoadSceneAsync("EndScreen", LoadSceneMode.Single);
      yield break;
    }

    // Load next scene
    yield return SceneManager.LoadSceneAsync(scenes[sceneIndex], LoadSceneMode.Additive);
    // Set up eveything again for new scene
    Init();
  }

  private void Init()
  {
    // Stop looking for closest gem
    if (closestGemCoroutine != null)
    {
      StopCoroutine(closestGemCoroutine);
    }

    // Find all left-over enemies and destroy them
    Enemy[] enemies = GameObject.FindObjectsOfType<Enemy>();
    foreach (Enemy enemy in enemies)
    {
      Destroy(enemy.gameObject);
    }

    // Find all left-over dropped items and destroy them
    DropObject[] dropObjects = GameObject.FindObjectsOfType<DropObject>();
    foreach (DropObject dropObject in dropObjects)
    {
      Destroy(dropObject.gameObject);
    }

    // Get scene config object
    sceneConfig = GameObject.Find("SceneConfig").GetComponent<SceneConfig>();

    enemyParent = GameObject.Find("Enemies");

    // Create all our gems at the locations provided
    gems = new List<GameObject>();
    for (int i = 0; i < sceneConfig.gemLocations.Length; i++)
    {
      // Create the gem and store reference in the list
      GameObject createdGem = Instantiate(sceneConfig.gemPrefab, sceneConfig.gemLocations[i], Quaternion.identity);
      gems.Add(createdGem);

      // Remove the gem from the list when it is picked up
      createdGem.GetComponent<GemController>().onGemCollected.AddListener(() =>
      {
        gems.Remove(createdGem);
      });
      
      // Spawn enemies randomly around the gem
      for (int j = 0; j < sceneConfig.enemiesPerGem; j++)
      {
        //creating each enemy in the levels
        InstantiateEnemy(sceneConfig.gemLocations[i] + ((Vector3)Random.insideUnitCircle * 5.0f));
      }
    }

    // Spawn additional enemies around the map
    for (int i = 0; i < sceneConfig.extraEnemyGroups; i++)
    {
      float x = Random.Range(-75, 95);
      float y = Random.Range(-70, 55);

      for (int j = 0; j < sceneConfig.enemiesPerGem; j++)
      {
        Vector3 groupPos = new Vector3(x, y, 0) + ((Vector3)Random.insideUnitCircle * 5.0f);
        if (Vector3.Distance(groupPos, sceneConfig.playerStartPos) < 20)
        {
          // Just ignore this position if it is too close to the player's starting pos
          continue;
        }
        InstantiateEnemy(groupPos);
      }
    }

    // Start looking for closest gem
    closestGemCoroutine = StartCoroutine(FindClosestGem());

    // Notify listeners that we loaded the level
    levelLoaded.Invoke();
  }

  private void InstantiateEnemy(Vector3 pos)
  {
    GameObject instance = Instantiate(sceneConfig.enemyPrefab, pos, Quaternion.identity, enemyParent.transform);
    // Enemies have 10% chance of dropping an item
    if (Random.value < 0.1f)
    {
      instance.GetComponent<Enemy>().dropItem = dropItems[Random.Range(0, dropItems.Length)];
    }
  }

  // Finds the closest gem to the player, used to show the player where to go
  private IEnumerator FindClosestGem()
  {
    while (true)
    {
      float closestGemDistance = float.MaxValue;
      GameObject closestGem = null;
      foreach (GameObject gem in gems)
      {
        float distance = Vector3.Distance(gem.transform.position, player.gameObject.transform.position);
        if (distance < closestGemDistance)
        {
          closestGemDistance = distance;
          closestGem = gem;
        }
      }

      ClosestGem = closestGem.transform.position - player.gameObject.transform.position;

      // Don't need to do this every tick
      yield return new WaitForSeconds(0.4f);
    }
  }
}
