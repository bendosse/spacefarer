using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
  private Vector3 direction;
  private int hits;

  public string LaunchedBy;
  public ProjectileItem item;

  public void Init(ProjectileItem item, Vector3 direction, string launchedBy)
  {
    this.item = item;
    this.direction = Vector3.Normalize(direction);
    this.direction.z = 0;
    GetComponent<SpriteRenderer>().color = item.projectileColor;
    this.LaunchedBy = launchedBy;
  }

  private void Update()
  {
    // Move the projectile
    transform.Translate(direction * item.projectileSpeed * Time.deltaTime);
  }

  private void OnTriggerEnter2D(Collider2D col)
  {
    // If the enemy shot the player
    if (col.CompareTag("Player") && LaunchedBy == "Enemy")
    {
      Player player = col.gameObject.GetComponent<Player>();
      if (player != null)
      {
        player.Damage(item.projectileDamage);
        
        // Projectile can only hit a certain amount of things
        // Keep track of how many it hits, destroy it when it reaches the cap
        hits++;

        if (hits >= item.projectileHits)
        {
          Destroy(gameObject);
        }
      }
    }
    else if (col.CompareTag("Enemy") && LaunchedBy == "Player") // The player shot the enemy
    {
      Enemy enemy = col.gameObject.GetComponent<Enemy>();
      if (enemy != null)
      {
        enemy.TakeDamage(item.projectileDamage);

        // Projectile can only hit a certain amount of things
        // Keep track of how many it hits, destroy it when it reaches the cap
        hits++;

        if (hits >= item.projectileHits)
        {
          Destroy(gameObject);
        }
      }
    }

    // The projectile hit something other than the 
    if (!col.CompareTag("Player") && !col.CompareTag("Enemy") && !col.CompareTag("Projectile"))
    {
      Destroy(gameObject);
    }
  }
}

