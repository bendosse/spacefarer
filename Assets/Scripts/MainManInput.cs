using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainManInput : MonoBehaviour
{
  public GameManager gameManager;

  private float xSpeed;
  private float ySpeed;
  private Animator animator;


  // Start is called before the first frame update
  void Start()
  {
    DontDestroyOnLoad(gameObject);

    animator = GetComponent<Animator>();

    //check that animator was found
    if (animator == null)
    {
      Debug.LogError("No animator found");
    }
    gameManager.levelLoaded.AddListener(() =>
    {
      transform.position = gameManager.sceneConfig.playerStartPos;

    });
  }

    Vector2 movement;


    // Update is called once per frame
    void Update()
  {
        //input
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("speed", movement.sqrMagnitude);

        float verticalAxis = Input.GetAxis("Vertical");
        float horizontalAxis = Input.GetAxis("Horizontal");

        xSpeed = 0;
        ySpeed = 0;

        if (Mathf.Abs(verticalAxis) > 0.01f)
        {
          ySpeed = gameManager.playerSpeed * verticalAxis * Time.deltaTime;
        }

        if (Mathf.Abs(horizontalAxis) > 0.01f)
        {
          xSpeed = gameManager.playerSpeed * horizontalAxis * Time.deltaTime;
        }

        transform.Translate(xSpeed, ySpeed, 0);
    }
}
