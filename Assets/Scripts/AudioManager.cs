using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
  // List of all the sounds we can play
  public List<AudioClip> audioClips;

  // Audio source on camera
  public AudioSource audioSource;

  // Play a sound by name
  public void PlaySound(string soundName)
  {
    foreach (AudioClip audioClip in audioClips)
    {
      if (audioClip.name == soundName)
      {
        audioSource.PlayOneShot(audioClip);
        return;
      }
    }
    Debug.LogError("AudioManager: Sound not found: " + soundName);
  }
}
