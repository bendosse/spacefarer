using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropObject : MonoBehaviour, IPointerClickHandler
{
  // Script for objects that enemies drop when they die

  // Reference to the player's inventory
  private Inventory inventory;

  public GameObject label;

  public Item item;

  public void Init(Item dropItem)
  {
    // Called when item dropped
    inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
    item = dropItem;
    label.GetComponent<TMPro.TMP_Text>().text = item.name;
  }

  public void OnPointerClick(PointerEventData eventData)
  {
    // Register a right click on the object, add it to the player's inventory
    if (eventData.button == PointerEventData.InputButton.Right)
    {
      inventory.AddItem(item);
      Destroy(gameObject);
    }
  }
}
