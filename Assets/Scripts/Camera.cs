using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
  public GameObject attachedTo;

  // Start is called before the first frame update
  void Start()
  {
    DontDestroyOnLoad(gameObject);
  }

  void Update()
  {
    // Just follow the player around
    if (attachedTo == null) return;
    transform.position = new Vector3(attachedTo.transform.position.x, attachedTo.transform.position.y, -1.0f);
  }
}
