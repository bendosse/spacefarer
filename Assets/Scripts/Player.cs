using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{

  private float health;
  private Animator anim;
  private GameObject projectileParent;

  public GameManager gameManager;
  public AudioManager audioManager;

  // Emits an event when player is damanged, passes the new health value
  public UnityEvent<float> OnPlayerHealthChanged;
  public GameObject projectilePrefab;
  public ProjectileItem equippedProjectile;
  public bool canShoot;

  // Start is called before the first frame update
  void Start()
  {
    anim = GetComponent<Animator>();
    health = gameManager.playerInitialHealth;
    equippedProjectile = new ProjectileItem("Default", true, Color.black, 8f, 3f, 1);

    projectileParent = GameObject.Find("Projectiles");
    canShoot = true;
  }

  // Update is called once per frame
  void Update()
  {
    if (Input.GetButtonDown("Fire1") && canShoot)
    {
      ShootProjectile();
    }
  }

  // Heal the player with a given amount of health
  public void Heal(float amount)
  {
    health += amount;
    if (health > gameManager.playerInitialHealth)
    {
      health = gameManager.playerInitialHealth;
    }
    OnPlayerHealthChanged.Invoke(health);
  }

  public void Damage(float damage)
  {
    health -= damage;
    audioManager.PlaySound("oof");
    if (health <= 0)
    {
      gameObject.SetActive(false);
      gameManager.playerDied.Invoke();
    }
    else
    {
      // Let the ui know
      OnPlayerHealthChanged.Invoke(health);
    }
  }

  void ShootProjectile()
  {
    Vector3 direction = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;

    GameObject projectile = Instantiate(projectilePrefab, transform.position + (direction * 0.1f), Quaternion.identity, projectileParent.transform);
    projectile.GetComponent<Projectile>().Init(equippedProjectile, direction, "Player");
    audioManager.PlaySound("shoot");
  }
}
