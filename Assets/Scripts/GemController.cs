using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GemController : MonoBehaviour
{
    private Gem gemData;
    public UI uiController;
    
    public UnityEvent onGemCollected;


    // Start is called before the first frame update
    void Start()
    {
        gemData = new Gem();
        uiController = GameObject.FindWithTag("UICanvas").GetComponent<UI>();
    }

    // Player has collided with a gem, pick it up
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player")) return;

        gemData.inInventory = true;
        collision.gameObject.GetComponent<Inventory>().AddItem(gemData);

        uiController.CollectGem();
        onGemCollected.Invoke();

        Destroy(gameObject);
    }
}
