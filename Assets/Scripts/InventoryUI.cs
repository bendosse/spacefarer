using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{

  public Player player;
  public Inventory playerInventory;

  public TMPro.TextMeshProUGUI equippedProjectileText;

  public GameObject itemList;

  public void Init()
  {
    foreach (Transform child in itemList.transform)
    {
      Destroy(child.gameObject);
    }

    equippedProjectileText.text = player.equippedProjectile.name;

    // Some not very good math to figure out where to position text
    RectTransform rt = itemList.GetComponent<RectTransform>();
    float itemWidth = rt.rect.width / 5;
    float itemHeight = rt.rect.height / 5;
    float xPos = rt.transform.position.x - (itemWidth / 2);
    float yPos = rt.transform.position.y - (itemHeight / 2);
    int i = 0;
    foreach (Item item in playerInventory.items)
    {
      GameObject itemObject = new GameObject();
      InventoryUIItem uIItem = itemObject.AddComponent<InventoryUIItem>();
      uIItem.item = item;
      uIItem.inventoryUI = this;
      TMPro.TextMeshProUGUI itemText = itemObject.AddComponent<TMPro.TextMeshProUGUI>();
      if (i % 5 == 0)
      {
        yPos -= itemHeight;
      }
      itemObject.transform.position = new Vector3(xPos + (i % 5) * itemWidth, yPos, 0);
      itemText.text = item.name;
      itemObject.transform.SetParent(itemList.transform);
      i++;
    }
  }
}
