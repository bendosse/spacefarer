using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Inventory : MonoBehaviour
{
  public Player player;
  public List<Item> items { get; private set; }

  public void AddItem(Item item)
  {
    items.Add(item);
  }

  // Check if the player has a certain number of items of a type
  public bool HasEnoughOfItemOfType(int numRequired, Type type)
  {
    int n = 0;
    foreach (Item item in items)
    {
      if (item.GetType() == type) n++;
    }
    return n >= numRequired;
  }

  // Check how many of a certain item the player has
  public int NumItemsOfType(Type type)
  {
    int n = 0;
    foreach (Item item in items)
    {
      if (item.GetType() == type) n++;
    }
    return n;
  }

  // Remove items of a certain type
  public void RemoveItemsOfType(Type type)
  {
    items.RemoveAll(item => item.GetType() == type);
  }

  // Use an item
  public void UseItem(Item item)
  {
    Item i = items.Find(i => i.name == item.name);
    if (i == null) return;
    i.Use(player);
    items.Remove(item);
  }

  // Start is called before the first frame update
  void Start()
  {
    items = new List<Item>();
  }
}
