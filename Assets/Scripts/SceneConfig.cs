using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneConfig : MonoBehaviour
{
  // Data storage for each level
  public Vector3 playerStartPos;

  public GameObject enemyPrefab;
  public int numGems;
  public int numGemsRequired;
  public int enemiesPerGem;
  public int extraEnemyGroups;

  public Vector3[] gemLocations;
  public GameObject gemPrefab;
  
}
