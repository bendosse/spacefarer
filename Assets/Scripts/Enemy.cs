using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
  private GameObject dropItemParent;
  private GameObject projectileParent;
  private ProjectileItem equippedProjectile;

  private bool attackCoroutineRunning = false;

  // Reference to the player
  public Player player;
  public float health;
  public float maxHealth;
  public float damage;
  public float speed;
  public Item dropItem;
  public GameObject healthBar;
  public GameObject dropItemPrefab;
  public GameObject projectilePrefab;
  public AudioManager audioManager;
  public GameManager gameManager;


  // Start is called before the first frame update
  void Start()
  {
    gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    healthBar.SetActive(false);

    // Empty parent to keep heirarchy clean
    dropItemParent = GameObject.Find("DroppedItems");
    projectileParent = GameObject.Find("Projectiles");

    audioManager = UnityEngine.Camera.main.GetComponent<AudioManager>();

    equippedProjectile = new ProjectileItem("Default", true, Color.red, 8f, damage, 1);
  }

  public void TakeDamage(float damage)
  {
    healthBar.SetActive(true);
    health -= damage;
    // Update ui
    healthBar.GetComponent<Image>().fillAmount = health / maxHealth;
    if (health <= 0)
    {
      Die();
    }
  }

  private void Die()
  {
    if (dropItem != null)
    {
      // Create the dropped item
      GameObject instance = Instantiate(dropItemPrefab, transform.position, Quaternion.identity, dropItemParent.transform);
      instance.GetComponent<DropObject>().Init(dropItem);
    }
    Destroy(gameObject);
  }

  public void AttackPlayer(string attackType)
  {
    if (!attackCoroutineRunning)
    {
      StartCoroutine(AttackPlayerCoroutine(attackType));
    }
  }

  // Using a coroutine to stop super frequent attacks
  private IEnumerator AttackPlayerCoroutine(string attackType)
  {
    attackCoroutineRunning = true;

    if (attackType == "Melee") // Melee attack
    {
      player.Damage(damage);
    }
    else // Ranged attack
    {
      // Direction to sent projectile
      Vector3 direction = player.transform.position - transform.position;

      // Create the projectile and give it the necessary data
      GameObject projectile = Instantiate(projectilePrefab, transform.position + (direction * 0.1f), Quaternion.identity, projectileParent.transform);
      projectile.GetComponent<Projectile>().Init(equippedProjectile, direction, "Enemy");
    }
    
    yield return new WaitForSeconds(1f);
    attackCoroutineRunning = false;
  }
}
