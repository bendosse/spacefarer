using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryUIItem : MonoBehaviour, IPointerClickHandler
{
  private Inventory inventory;

  public Item item;
  public Player player;

  public InventoryUI inventoryUI;

  private void Start()
  {
    player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    inventory = player.GetComponent<Inventory>();
  }

  public void OnPointerClick(PointerEventData eventData)
  {
    Debug.Log("Clicked on " + item.name);
    if (eventData.button == PointerEventData.InputButton.Right)
    {
      if (item != null)
      {
        inventory.UseItem(item);
        inventoryUI.Init();
      }
    }
  }
}
