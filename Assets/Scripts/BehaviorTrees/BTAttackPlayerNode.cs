using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BTAttackPlayerNode : BTNode
{
  private Enemy enemy;
  private string attackType;
  
  public BTAttackPlayerNode(BehaviorTree tree) : base(tree)
  {
    enemy = tree.Blackboard["Enemy"] as Enemy;
    attackType = tree.Blackboard["AttackType"] as string;
  }

  public override Result Execute()
  {
    // Use enemy's attack method to handle the attack
    enemy.AttackPlayer(attackType);
    return Result.Success;
  }
}