using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
  Using the code directly from class for this.
*/

public class BehaviorTree : MonoBehaviour
{

  private BTNode mRoot;
  private bool startedBehavior;

  // BTNode is the root node, while our Dictionary acts as 
  // the blackboard so that nodes can communicate with each other
  public BTNode Root { get { return mRoot; } }
  public Dictionary<string, object> Blackboard { get; set; }

  public string AttackType;
  public Enemy enemy;
  public float minDistance;

  // Use this for initialization
  void Start()
  {
    // Adding references to stuff we will need.
    Blackboard = new Dictionary<string, object>();
    Blackboard.Add("Player", GameObject.FindGameObjectWithTag("Player").GetComponent<Player>());
    Blackboard.Add("AudioManager", UnityEngine.Camera.main.GetComponent<AudioManager>());
    Blackboard.Add("Enemy", enemy);
    Blackboard.Add("MinDistance", minDistance);
    Blackboard.Add("Target", GameObject.FindGameObjectWithTag("Player"));
    Blackboard.Add("Range", 10f);
    Blackboard.Add("AttackType", AttackType);

    startedBehavior = false;

    // Enemy should wait till the player is in range, then chase them to a distance, then attack.
    mRoot = new BTRepeaterNode(this, new BTSequencerNode(this, 
                  new BTNode[] {
                    new BTWaitForObjectInRangeNode(this),
                    new BTMoveToTargetNode(this),
                    new BTAttackPlayerNode(this)
                    }));
  }

  // Update is called once per frame
  void Update()
  {
    if (!startedBehavior)
    {
      StartCoroutine(RunBehavior());
      startedBehavior = true;
    }
  }

  IEnumerator RunBehavior()
  {
    BTNode.Result result = Root.Execute();
    while (result == BTNode.Result.Running)
    {
      yield return null;
      result = Root.Execute();
    }
  }
}