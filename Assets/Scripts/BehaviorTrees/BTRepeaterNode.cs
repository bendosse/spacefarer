using UnityEngine;
using System.Collections;

/**
  Using the code directly from class for this.
*/

public class BTRepeaterNode : BTDecoratorNode
{

  public BTRepeaterNode(BehaviorTree t, BTNode child) : base(t, child)
  {
  }

  public override Result Execute()
  {
    Child.Execute();
    return Result.Running;
  }
}