using UnityEngine;
using System.Collections;

public class BTWaitForObjectInRangeNode : BTNode
{
  private GameObject obj;
  private float range;

  public BTWaitForObjectInRangeNode(BehaviorTree tree) : base(tree)
  {
    obj = tree.Blackboard["Target"] as GameObject;
    range = (float) tree.Blackboard["Range"];
  }

  public override Result Execute()
  {
    // Check if target is in a range
    if (Vector3.Distance(Tree.gameObject.transform.position, obj.transform.position) < range)
    {
      return Result.Success;
    }
    else
    {
      return Result.Running;
    }
  }
}