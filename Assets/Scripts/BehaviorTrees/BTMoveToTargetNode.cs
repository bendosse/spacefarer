using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BTMoveToTargetNode : BTNode
{
  private Enemy enemy;
  private Player player;

  private float minDistance;
  protected Vector3 targetPosition;

  public BTMoveToTargetNode(BehaviorTree tree) : base(tree)
  {
    enemy = tree.Blackboard["Enemy"] as Enemy;
    player = tree.Blackboard["Player"] as Player;
    minDistance = tree.Blackboard["MinDistance"] as float? ?? 0.1f;
  }

  public override Result Execute()
  {
    // Update our transform
    targetPosition = (Tree.Blackboard["Target"] as GameObject).transform.position;
    // if we've made it to the destination
    if (Vector3.Distance(Tree.gameObject.transform.position, targetPosition) < minDistance)
    {
      return Result.Success;
    }
    else
    {
      // move towards the target
      Tree.gameObject.transform.position =
          Vector3.MoveTowards(Tree.gameObject.transform.position,
          player.gameObject.transform.position, Time.deltaTime * enemy.speed);
      return Result.Running;
    }

  }
}